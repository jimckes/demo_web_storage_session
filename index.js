function guardarEnSessionStorage() {
  var txtClave = document.getElementById("txtClave"); /* Referencia al input de clave */
  var txtValor = document.getElementById("txtValor"); /* Referencia al input de valor */
  var clave = txtClave.value;
  var valor = txtValor.value;
  sessionStorage.setItem(clave, valor);
  var objeto = {
    nombre:"Ezequiel",
    apellidos:"Llarena Borges",
    ciudad:"Madrid",
    pais:"España"
  };
  //sessionStorage.setItem("json", JSON.stringify(objeto));

  displayContactsAsATable("listaDiv");
  
}
function leerDeSessionStorage() {
  var txtClave = document.getElementById("txtClave"); /* Referencia al input de clave */
  var clave = txtClave.value;
  var valor = sessionStorage.getItem(clave);
  var spanValor = document.getElementById("spanValor");
  spanValor.innerText = valor;

  //var listaSessionStorage= sessionStorage.getItems();
  displayContactsAsATable("listaDiv");

  /*var datosUsuario = JSON.parse(sessionStorage.getItem("json"));
  console.log(datosUsuario.nombre);
  console.log(datosUsuario.pais);
  console.log(datosUsuario);*/
}

function eliminarSessionStorage() {
  var txtClave = document.getElementById("txtClave"); /* Referencia al input de clave */
  sessionStorage.removeItem(txtClave.value);
  displayContactsAsATable("listaDiv");
}

function limpiarSessionStorage() {
  //var txtClave = document.getElementById("txtClave"); /* Referencia al input de clave */
  sessionStorage.clear();
  displayContactsAsATable("listaDiv");
  
}

function displayContactsAsATable(idOfContainer) {
  // empty the container that contains the results
    let container = document.querySelector("#" + idOfContainer);
    container.innerHTML = "";

  
  /*if(this.sessionStorage.length === 0) {
    container.innerHTML = "<p>Nothing to display!</p>";
    // stop the execution of this method
    return;
  } */ 

    // creates and populate the table with users
    var table = document.createElement("table");
        
    // iterate on the array of users
   // sessionStorage.forEach(function(currentContact) {
        // creates a row
     //   var row = table.insertRow();
      
    //row.innerHTML = "<td>" + currentContact.clave + "</td>"
     //       + "<td>" + currentContact.valor + "</td>"
     //});

     //for (i in sessionStorage) {
      //document.write(sessionStorage[i]);
      //var row = table.insertRow();
      
      //row.innerHTML = "<td>" + sessionStorage[i] + "</td>"
      //      + "<td>" +  sessionStorage.valor + "</td>"
      //}

      for (x=0; x<=sessionStorage.length-1; x++)  {  
        clave = sessionStorage.key(x); 
        //document.write("La clave " + clave + "contiene el valor " + localStorage.getItem(clave) + "
      //"); 
      var row = table.insertRow();
      
          row.innerHTML = "<td>" + clave + "</td>"
            + "<td>" +  sessionStorage.getItem(clave) + "</td>" 
      }

     // adds the table to the div
     container.appendChild(table);
  }